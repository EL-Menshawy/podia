(function($, document, window){
	
	$(document).ready(function(){

		// Cloning main navigation for mobile menu
		$(".mobile-navigation").append($(".main-navigation .menu").clone());

		// Mobile menu toggle 
		$(".menu-toggle").click(function(){
			$(".mobile-navigation").slideToggle();
		});
	});

	$(window).load(function(){
		$(".feature-slider").flexslider({
			directionNav: true,
			controlNav: false,
			prevText: '<i class="fa fa-angle-left"></i>',
			nextText: '<i class="fa fa-angle-right"></i>',
		});
	});

})(jQuery, document, window);

mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}


function myFunction(){
	var name = document.getElementById('cName').value;
	var number = document.getElementById('pNumber').value;
	var value = document.getElementById('value').value;
	document.getElementById("err").innerHTML = "";
	
	if(name == ""){
		document.getElementById("err").innerHTML = "برجاء ملء الاسم";
		return false;
	}
	if(number == ""){
		document.getElementById("err").innerHTML = "برجاء ملء الرقم";
		return false;
	}
	if(number.length != 11 ){
		document.getElementById("err").innerHTML = "برجاء ملء يجب ان يكون ١١ رقم";
		return false;
	}
	if(number[0] != 0 && number[0] != '٠'){
		document.getElementById("err").innerHTML = "صيغة رقم خاطئة";
		return false;
	}
	if(number[1] != 1 && number[1] != '١'){
		document.getElementById("err").innerHTML = "صيغة رقم خاطئة";
		return false;
	}
	if(number[2] != 1  && number[2] != 2 && number[2] != 0 && number[2] != '١'  && number[2] != '2' && number[2] != '٠'){
		document.getElementById("err").innerHTML = "صيغة رقم خاطئة";
		return false;
	}
	if (value == 'المنطقة') {
		document.getElementById("err").innerHTML = "برجاء اختيار المنطقة";
		return false;
	}
	var mail = 'https://royaldevelopments-eg.com//landing/mail.php?name='+name+'&number='+number+'&type='+value ;
	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", mail, false);
	xhttp.send( null );
	alert(xhttp.responseText);
// 	document.getElementById("err").innerHTML = ;
(function(E,n,G,A,g,Y,a){E['EngLandingObject']=g;E[g]=E[g]||function(){ (E[g].q=E[g].q||[]).push(arguments)},E[g].l=1*new Date();Y=n.createElement(G), a=n.getElementsByTagName(G)[0];Y.async=1;Y.src=A;a.parentNode.insertBefore(Y,a)})(window,document,'script','//widget.engageya.com/eng_landing.js','__engLanding');__engLanding('createPixel',{pixelid:213216,dtms:0});

}